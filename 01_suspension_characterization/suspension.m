clc 
close all 
clear all

%% Load matrix
load('data/suspensionA50.mat');   % Data suspension test with Amplitude of 50 mm
load('data/suspensionA40.mat');   % Data suspension test with Amplitude of 40 mm
load('data/suspensionA25.mat');   % Data suspension test with Amplitude of 25 mm
load('data/suspensionA10.mat');   % Data suspension test with Amplitude of 10 mm
load('data/suspensionA5.mat');    % Data suspension test with Amplitude of 5 mm
load('data/suspensionA2.mat');    % Data suspension test with Amplitude of 2 mm

% Saving strategy of the matrix: A= amplitude F= frequency C=click
% Amplitude (A) can be 2,5,10,25,40,50 mm
% Frequency (F) can be 0.1, 0.5, 1, 5 Hz

%% Speed function of the force
sf_05=@(t) 2*pi*0.5*cos(2*pi*0.5*(t-1));    % speed function with frequency=0.5 Hz
dt=0.0002;
offset = 41.0;

%% General model
% linear symmetrical
Foffset_linear =    -0.4056;    % (-0.4056, -0.4056)
Fstatic_linear =    0.04693;    % (0.0469, 0.04696)
Kfriction_linear =  4.191;      % (4.165, 4.218)
c_linear =          0.0001733 ; % (0.0001727, 0.0001739)
k_linear =          0.007895 ;  % (0.007894, 0.007897)
fl = @(x,y) Foffset_linear + Fstatic_linear*tanh(Kfriction_linear.*y)+k_linear.*x+c_linear.*y;
%Coefficients (with 95% confidence bounds)

%% Recompute raw measures to get positions and forces as positive
pos_2 = -(A2F05C0(:,2) - offset);
pos_5 = -(A5F05C0(:,2) - offset);
pos_10 = -(A10F05C0(:,2) - offset);
pos_25 = -(A25F05C0(:,2) - offset);
pos_40 = -(A40F05C0(:,2) - offset);
pos_50 = -(A50F05C0(:,2) - offset);

force_2 = -(A2F05C0(:,3));
force_5 = -(A5F05C0(:,3));
force_10 = -(A10F05C0(:,3));
force_25 = -(A25F05C0(:,3));
force_40 = -(A40F05C0(:,3));
force_50 = -(A50F05C0(:,3));

%% COMPARISON SIMUATION VS MEASURE for 0.5 Hz
fig1 = figure;

hold on
p1 = plot3(pos_2,-2*sf_05(A2F05C0(:,1)),force_2, 'Color', 'b', 'LineWidth',1);
plot3(pos_5,-5*sf_05(A5F05C0(:,1)),force_5, 'Color', 'b', 'LineWidth',1)
plot3(pos_10,-10*sf_05(A10F05C0(:,1)),force_10, 'Color', 'b', 'LineWidth',1)
plot3(pos_25,-25*sf_05(A25F05C0(:,1)),force_25, 'Color', 'b', 'LineWidth',1)
plot3(pos_40,-40*sf_05(A40F05C0(:,1)),force_40, 'Color', 'b', 'LineWidth',1)
hold off

hold on
p2 = plot3(pos_2,-2*sf_05(A2F05C0(:,1)),-fl(A2F05C0(:,2),2*sf_05(A2F05C0(:,1))), 'Color', 'r', 'LineWidth',1);
plot3(pos_5,-5*sf_05(A5F05C0(:,1)),-fl(A5F05C0(:,2),5*sf_05(A5F05C0(:,1))), 'Color', 'r', 'LineWidth',1)
plot3(pos_10,-10*sf_05(A10F05C0(:,1)),-fl(A10F05C0(:,2),10*sf_05(A10F05C0(:,1))), 'Color', 'r', 'LineWidth',1)
plot3(pos_25,-25*sf_05(A25F05C0(:,1)),-fl(A25F05C0(:,2),25*sf_05(A25F05C0(:,1))), 'Color', 'r', 'LineWidth',1)
plot3(pos_40,-40*sf_05(A40F05C0(:,1)),-fl(A40F05C0(:,2),40*sf_05(A40F05C0(:,1))), 'Color', 'r', 'LineWidth',1)
view(330, 45)
hold off

grid on
set(gcf,'color', 'w');
set(gca,'fontsize', 22);
% title('Load-Amplitude-Speed Suspension')
xlabel('Amplitude (mm)')
ylabel('Speed (mm/s)')
zlabel('Load (kN)')
legend([p1 p2], {'Measure', 'Simulation'}, 'Location', 'east')

fig1.WindowState = 'maximized';
savefig('picture/Load_Amplitude_Speed_Suspension_0.5_Hz.fig');
saveas(fig1, 'picture/Load_Amplitude_Speed_Suspension_0.5_Hz.png');

%% COMPARISON SIMUATION VS MEASURE for 0.5 Hz 50mm amplitude
fig2 = figure;

hold on
plot(pos_50, force_50, 'Color', 'b', 'LineWidth',1);
hold off

% Saturation at 41mm for the model
amplitude = A50F05C0(:,2);
amplitude(amplitude > offset) = offset;
speed = movmean([0; diff(amplitude) / dt], 30);

hold on
plot(pos_50, -fl(amplitude, speed), 'Color', 'r', 'LineWidth',1);
hold off

grid on
set(gcf,'color','w');
set(gca,'fontsize', 22);
% title('Amplitude-Load Suspension')
xlabel('Amplitude (mm)')
ylabel('Load (kN)')
legend({'Measure', 'Simulation'}, 'Location', 'east')

fig2.WindowState = 'maximized';
savefig('picture/Load_Amplitude_Speed_Suspension_0.5_Hz_amp_50.fig');
saveas(fig2, 'picture/Load_Amplitude_Suspension_0.5_Hz_amp_50.png');
