# Development of a Hardware-in-the-Loop Test Bench for Validation of an ABS on an e-Bike

This work presents the development of a Hardware-in-the-Loop (HIL) test bench that can be used to validate an electric bicycle (e-bike) anti-lock braking system (ABS) for different test scenarios. The approach involves interfacing a virtual bicycle simulation model running on a real-time target machine with the physical ABS hardware under test. This setup allows to test and evaluate ABS behavior in a safe and reproducible way, before starting testing on the track.


## Referenced e-Bike

The referenced e-Bike is the Flyer Goroc 2, a crossover type bike, 29-inch wheel sets with 2.6-inch all-terrain tires and equipped with a 120mm suspension fork. It weights 26kg and its permissible load is 150kg.
A documentation is available [here](00_documentation/FLYER_E-Bikes_MY20_EUR_E_Goroc2.pdf) and the dimensions of the bike are available [here](00_documentation/Dimensions_FLYER_Goroc_4.30.JPG).

## Bike parameters

The parameters used to generate the simulation results presented in the original paper are given by this table:

| Title of the parameter                                        | Value | Unit  |
|---------------------------------------------------------------|-------|-------|
| Mass of "main body"                                           | 100   | kg    |
| Mass of the front wheel                                       | 3     | kg    |
| Mass of the rear wheel                                        | 3     | kg    |
| Inertia of "main body"                                        | 15    | kgm2  |
| Inertia of the front wheel                                    | 0.2   | kgm2  |
| Inertia of the rear wheel                                     | 0.2   | kgm2  |
| Wheelbase                                                     | 1.212 | m     |
| Horizontal length from the front wheel axis to CoG            | 0.35  | m     |
| Height from the front wheel axis to CoG at static equilibrium | 0.7   | m     |
| Front wheel radius at static equilibrium                      | 0.372 | m     |
| Rear wheel radius at static equilibrium                       | 0.372 | m     |
| Caster angle of the front suspension                          | 22    | °     |
| Stiffness of the equivalent vertical front suspension         | 6790  | N/m   |
| Damping of the equivalent vertical front suspension           | 200   | Ns/m  |
| Coulomb friction in the equivalent vertical front suspension  | 46.93 | N     |
| Numerical gain for the tanh function                          | 4.175 | -     |
| Stiffness of the front tire                                   | 138200| N/m   |
| Damping of the front tire                                     | 1000  | Ns/m  |
| Stiffness of the rear tire                                    | 138200| N/m   |
| Damping of the rear tire                                      | 1000  | Ns/m  |
| Effective diameter disc brake                                 | 0.091 | m     |
| Coefficient of friction brake pad                             | 0.53  | -     |
| Pacejka coefficient B                                         | 4.28  | -     |
| Pacejka coefficient C                                         | 1.72  | -     |
| Pacejka coefficient D                                         | 0.95  | -     |
| Pacejka coefficient E                                         | 0     | -     |

<!--- Additional parameters used mainly in AMESim are given by this table:

| Title of the parameter                                        | Value | Unit  |
|---------------------------------------------------------------|-------|-------|
| Initial longitudinal velocity                                 | 0     | km/h  |
| Rolling coefficient                                           | 0.015 | -     |
| Longitudinal slip stiffness tire                              | 0.07  | -     |
| Striction friction coefficient tire                           | 0.95  | -     |
| Friction coefficient tire                                     | 0.4   | -     |
| Gravity constant                                              | 9.806 | m/s2  |


## Driven inputs

The driven inputs can driven during the whole simulation, here is the list of these parameters:

| Title of the parameter                                        | Unit  |
|---------------------------------------------------------------|-------|
| Driving torque on the rear wheel                              | Nm    |
| Force on the front brake pad                                  | N     |
| Force on the rear brake pad                                   | N     |
| Adherence of the road                                         | -     |


#### Outputs of the simulations

The output of the simulation is the response of the system. It will help to compare within the measures. The table gives the different outputs:

| Title of the parameter                                        | Unit  |
|---------------------------------------------------------------|-------|
| Acceleration of G along X-axis                                | m/s2  |
| Acceleration of G along Z-axis                                | m/s2  |
| Velocity of G along X-axis                                    | m/s   |
| Velocity of G along Z-axis                                    | m/s   |
| Displacement of G along X-axis                                | m     |
| Displacement of G along Z-axis                                | m     |
| Pitch rate                                                    | rad/s |
| Pitch angle                                                   | rad   |
| Displacement of the front wheel along Z-axis                  | m     |
| Angular velocity of the front wheel                           | rad/s |
| Angular velocity of the rear wheel                            | rad/s |
| Angular position of the front wheel                           | rad   |
| Angular position of the rear wheel                            | rad   |
| Torque at the front wheel                                     | Nm    |
 --->

## Content
<!---
#### 01. AMESim model

The folder contains a standalone bike's model made with [AMESim V2022.1](https://plm.sw.siemens.com/en-US/simcenter/systems-simulation/amesim/) software, a measurement reader made with AMESim and some data files from a measure with the real e-bike.

The AMESim model is the step that has permitted to give an initial approximation of the e-Bike parameters.
 --->
#### 01. Suspension characterization

The folder contains the script and the raw data to charaterize the front suspension of the e-bike ([SR Suntour Raidon34](https://www.srsuntour.com/fr/FR/produits/fourche/Raidon-34-5859.html), 120mm, 15x110mm Boost).

The script has been made with [Matlab 2020b](https://ch.mathworks.com/?s_tid=gn_logo). It is used to generate the Figure 6 from the original paper.
<!---
#### 03. Brake profile for the simulation

The folder contains the notebook to extract the brake curve from a measurement in order to replay the braking phase in the simulation. Once the .mat file generated, it can be reused for the 04_amesim_model_simulink_desktop.

The script has been made with Python and it requires the libraries pandas, numpy, plotly and scipy.

#### 04. AMESim model with a simulink real-time desktop

The folder contains the bike's model edited with AMESim coupled with a simulink interface that allow to play a sceanrio and to visualize the bike's motion during the braking phase.

The script reused the AMESim model and add an interface to communicate with Simulink. The Simulink environment has been set to read the braking profile previously made and displaying the bike's motion. Simulink requires the library Simscape Multibody for the animation.

Few steps to use the model:
- Open the model in AMESim
- Click on the "Simulation" tab, select "Interfaces >> Generate files for Real-Time...", choose "Simulink Real-Time (xPC):xpc :obj" platform and click "Generate".
- Open Matlab with "Tools >> Matlab" (Few AMESim initialization lines should be displayed)
- Open "Simulink" from Matlab
- Open "Bike_model_wo_speedgoat.slx" from Simulink
- On the "SIMULATION" tab, click "Run (Paced)"
- Save the simulation results with the "Data Inspector"
 --->
#### 02. Simulink bike model 

The folder contains the bike's model edited with Simulink coupled with a Matlab model configuration.

To run the model, follow the few step:
- Open Matlab with bike_model.m
- Start the script and set the parameters such as: *Select conditions of the ground: 5*, *Select an initial speed: 25*, *How many kilos does the rider weigh: 70*
- Open Simulink with bike_model_simulink.slx
- Set the system target file as grt.tlc (initially sldrt.tlc) (*Modeling >> Model Settings >> Code Generation -> System target file*)
- Launch the simulation by "Simulation >> Run" (Stop time = 7 seconds)
- Open the Data Inspector to visualize the results


#### 03. Measurements data

The folder contains the scripts to compare a measurement to a simulation through the front wheel speed, the x-acceleration of the bike, the fork displacement and the pitch rate of the bike.  

<!--- A script has been made with Python and it requires the libraries pandas, numpy, plotly and scipy. --->

The other script is a Matlab script that requires the output from the simulation with Simulink model as .mat.

## Download a project from Gitlab

Open a folder location to store the project, open a [Git bash](https://git-scm.com/downloads) on this location and type the command:

```powershell
git clone https://gitlab.forge.hefr.ch/nicolas.ramosaj/bmd2023-ramosaj-fusco-viennet.git
```	

## Related proceeding

- *Ramosaj, N., Fusco, C. & Viennet, E.* (2023). 
**Development of a Hardware-in-the-Loop Test Bench for Validation of an ABS System on an e-Bike.**
The Evolving Scholar - BMD 2023, 5th Edition. 
[DOI:10.59490/6504843acc05ff8f18f36576](https://doi.org/10.59490/6504843acc05ff8f18f36576)
