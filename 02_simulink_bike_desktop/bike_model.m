clc
clear
close all

%% parameters
mb_1=30;                        % Mass of "main body + rear wheel" (kg)
Ib=15;                          % Inertia of "main body + rear wheel" (kgm2)
Iwheel=0.2;                     % Inertia of front and rear wheel (kgm2)
mwheel=3.0;                     % Mass of the front wheel (kg)
Xg=0.35;                        % Horizontal length from the front wheel axis to CoG (m)
b=Xg;                           % 
Yg=0.7;                         % Height from the front wheel axis to CoG at static equilibrium (m)
h=Yg;
p=1.212;                        % wWheelbase (m)
Rwheel=0.372;                   % Front and rear wheel radius at static equilibrium (m)
Rdisc=0.091;                    % Effective diameter disc brake (m)
mudisc=0.53;                    % Coefficient of friction brake pad (-)
Rres0=0.0;                      % rolling coefficient constant (-) (initially 0.015)
Rres2=0.0;                      % rolling coefficient constant ((s/rad)^2) (initially 0.00000648)
%gl=50;                         % positive boundary for translational hard stop 
gn=-50;                         % negative boundary for translational hard stop 
g1 = 80;
%Kwheel=82000;                  % Stiffness of the front and rear tire (1 bar) (N/m)
Kwheel=138200;                  % Stiffness of the front and rear tire (3 bar) (N/m)
Cwheel=1000;                    % Damping of the front and rear tire (Ns/m)
g=9.81 ;                        % gravity (m/s2)
% coefficient of front suspension for linear symmetrical suspension characterization
%    Fstatic=0.04800  ;         % F static friction force (kN) 
%    kfriction=4.187 ;          % coefficient friction force (coefficient for tanh)
%    c=0.0004156  ;             % suspension damping (kN/(mm/s)) 
%    k=0.00780  ;               % suspension stiffness (kN/mm) 

%c = 0.0001685 ;                % damping factor for negative relative speed (kN*s/mm)
c = 0.0002 ;                    % damping factor for negative relative speed (kN*s/mm)
c2 = c ;                        
%c2 = 0.0001822 ;               % damping factor for positive relative speed (kN*s/mm)
fc = 0.04693;                   % Coulomb friction in the equivalent vertical front suspension (kN)
foffset = -0.4148;              % F offset for measurement
%k = 0.005095 ;                 % first order stiffness for negative displacement (kN/mm)
%k2 = 0.008484 ;                % first order stiffness for positive displacement (kN/mm)
k2 = 0.00679 ;
k = k2 ;
%k3 = 2.133e-06;                % third order stiffness for negative displacement (kN/mm^3)
k3 = 0;
%k4 = -1.707e-07;               % third order stiffness for positive displacement (kN/mm^3)
k4 = 0;
kf = 4.175 ;                    % Numerical gain for the tanh function (-)


% General model:
f =@(x,y) (y<0).*(c.*y)+(y>=0).*(c2.*y)+foffset+fc.*tanh(kf.*y)+(x<0).*(k.*x+k3.*x.^3)+(x>=0).*(k2*x+k4.*x.^3);


%% ground selection
condition=1;
ground_selection='Select the condition of the ground: \n 1 Dry tarmac [default]\n 2 Wet tarmac\n 3 Snow\n 4 Ice\n 5 User defined\n';
gs=input(ground_selection, 's');
while condition==1
if isempty(gs) || gs=='1'
 B=10;
 C=1.9;
 D=1;
 E=0.97;
 condition=0;
 disp('You selected dry tarmac')
elseif gs=='2'
 B=12;
 C=2.3;
 D=0.82;
 E=1;
 condition=0;
 disp('You selected wet tarmac')
elseif gs=='3'
 B=5;
 C=2;
 D=0.3;
 E=1;  
 condition=0;
 disp('You selected snow')
elseif gs=='4'
 B=4;
 C=2;
 D=0.1;
 E=1; 
 condition=0;
 disp('You selected ice')
elseif gs=='5'
 B=4.28;
 C=1.72;
 D=0.95;
 E=0; 
 condition=0;
 disp('You selected your own parameters')
else 
    disp('Sorry, that value is not within the acceptable range');
	gs = input('Please input a value that is within the acceptable range: \n ', 's');
end
end % Pacejka magic formula street constant

%% initial speed selection 
condition_2=0;
is=input('Select an initial speed between 1 and 50 [km/h] [default 25 km/h] \n','s');
Initial_speed = str2double(is);
while condition_2==0

if ((Initial_speed >= 1) && (Initial_speed < 50))
    fprintf('You selected an initial speed of: %d km/h.\n', Initial_speed);
    condition_2=1;
elseif isempty(is)
    
    Initial_speed=7*3.6;
    fprintf('You selected the default speed of:%d km/h.\n', Initial_speed)
    condition_2=1;
else
    disp('Sorry, that value is not within the acceptable range. Please input a value that is within the acceptable range.')
	is=input('Select an initial speed between 1 and 50 [km/s] \n','s');
    Initial_speed = str2double(is);
    
end
end % select initial speed default for simulation
Initial_speed=Initial_speed/(3.6);

%% Mass of the rider
condition_2=0;
mr=input('How many kilos does the rider weigh [Kg]? [default 70 Kg] Please input a realistic value. \n','s');
mrider = str2double(mr);
while condition_2==0
    if isempty(mr)
        mrider=70;
        fprintf('The rider weight is: %d Kg \n', mrider);  
        condition_2=1;
    elseif isnan(mrider)
        mr=input('Sorry, this value is not acceptable. Please try again. How many kilos does the rider weigh [Kg]? [default 70 Kg]  \n','s');
        mrider = str2double(mr);
        condition_2=0;
    else 
        fprintf('The rider weight is: %d Kg \n', mrider);
        condition_2=1;
    end
end % select mass of the rider
mb=mb_1+mrider;
mtot=mb+mwheel*2;

%% simulazione
load('bikeflatroad.mat');
Brake_force_lever=BIKE_SIMULATION_1(:,3);
Force_Brake_Pad=BIKE_SIMULATION_1(:,2);
Time=BIKE_SIMULATION_1(:,1)-13.5;
Acceleration=BIKE_SIMULATION_1(:,6);
Pitch=BIKE_SIMULATION_1(:,10);
Speed_FW=BIKE_SIMULATION_1(:,12);
Speed_RW=BIKE_SIMULATION_1(:,5);
Fork_compression=BIKE_SIMULATION_1(:,4);