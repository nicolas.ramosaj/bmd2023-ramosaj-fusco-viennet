clc 
close all 
clear all

%% Load matrix
load('data/figure_7.mat');   % Data from simulation and measurement
time = data{1}.Values.Data(1:6001);
force_pad = data{2}.Values.Data(301:6301);
sim_fork_comp = data{3}.Values.Data(301:6301);
real_wh_speed = data{4}.Values.Data(301:6301);
sim_wh_speed = data{5}.Values.Data(301:6301);
sim_pitch = data{6}.Values.Data(301:6301);
real_acc = data{7}.Values.Data(301:6301);
real_pitch = data{8}.Values.Data(301:6301);
real_fork_comp = data{9}.Values.Data(301:6301);
sim_acc = data{10}.Values.Data(301:6301);

%% COMPARISON SIMUATION VS MEASURE
fig = figure;

hold on
subplot(2,2,1);
yyaxis('left')
plot(time, real_wh_speed, 'b', time, sim_wh_speed, 'r-', 'LineWidth', 1);
xlabel('Time (s)')
ylabel('Rotary speed (rpm)')
ylim([-25, 225]);
set(gca, 'YColor', 'k')

yyaxis('right')
plot(time, force_pad, 'Color', [0, 0.5, 0], 'LineWidth', 1);
grid on
set(gcf,'color', 'w');
set(gca,'fontsize', 14);
title('Front Wheel Speed and Force in Brake Caliper')
xlabel('Time (s)')
ylabel('Force (N)')
ylim([-300, 3000]);
lgd = legend({'Measure - Speed', 'Simulation - Speed', 'Measure - Force'}, 'Location', 'northeast');
lgd.FontSize = 10;
set(gca, 'YColor', 'k')

subplot(2,2,2);
plot(time, real_acc, 'b', time, sim_acc, 'r', 'LineWidth', 1);
grid on
set(gcf,'color', 'w');
set(gca,'fontsize', 14);
title('Bike X Acceleration')
xlabel('Time (s)')
ylabel('Acceleration (m/s^2)')
lgd = legend({'Measure', 'Simulation'}, 'Location', 'northeast');
lgd.FontSize = 10;

subplot(2,2,3);
plot(time, real_fork_comp, 'b', time, sim_fork_comp, 'r', 'LineWidth', 1);
grid on
set(gcf,'color', 'w');
set(gca,'fontsize', 14);
title('Fork Displacement')
xlabel('Time (s)')
ylabel('Displacement (mm)')
lgd = legend({'Measure', 'Simulation'}, 'Location', 'northeast');
lgd.FontSize = 10;

subplot(2,2,4);
plot(time, real_pitch, 'b', time, sim_pitch, 'r', 'LineWidth', 1);
grid on
set(gcf,'color', 'w');
set(gca,'fontsize', 14);
title('Bike Pitch Rate')
xlabel('Time (s)')
ylabel('Pitch Rate (°/s)')
lgd = legend({'Measure', 'Simulation'}, 'Location', 'northeast');
lgd.FontSize = 10;

fig.WindowState = 'maximized';
savefig('output/figure_7.fig');
saveas(fig, 'output/figure_7.png');